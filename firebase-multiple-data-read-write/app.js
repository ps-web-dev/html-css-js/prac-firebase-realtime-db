$(document).ready(()=>{
    var rootRef = firebase.database().ref().child('todos');

    rootRef.on('child_added', snap => {
        var content = snap.child('content').val();
        var id = snap.child('id').val();
        var status = snap.child('status').val();
        var statusText = "Pending";
        var statusClass = "pending"
        if(status==1){
            statusText = "Completed";
            statusClass = "completed"
        }
        $('.main-container').append(createTodoItem(id, statusClass, content));
    });
});

function createTodoItem(id, statusClass, content){
   return `<div id="${id}" class="${statusClass} sub-container">
                <div class="todo-container">
                    <span class="todo-text">${content}</span>
                    <div class="btn edit-btn">
                        <img src="./assets/ic-edit.png"/>
                        <span class="btn-text">Edit</span>
                    </div>
                    <div class="btn remove-btn">
                        <img src="./assets/ic-remove.png"/>
                        <span class="btn-text">Delete</span>
                    </div>
                </div>
            </div>`
}